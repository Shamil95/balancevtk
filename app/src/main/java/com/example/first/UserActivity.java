package com.example.first;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import com.example.first.Retrofit.NetworkService;
import com.example.first.User.BalanceModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity {
    Intent intent;
    UserModel user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_screen);

        intent = getIntent();
        user = new UserModel("0", intent.getIntExtra("lte", 0), intent.getStringExtra("ID"),
                intent.getStringExtra("FIO"), intent.getStringExtra("DateCreate"), intent.getStringExtra("LastCheckDate"));

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Проверка баланса, подождите");
        progressDialog.show();

        final ViewDataBinding binding = DataBindingUtil.setContentView(this, R.layout.user_screen);
        NetworkService.getInstance().retrofitHelper
                .getBalance(user.ID)
                .enqueue(new Callback<BalanceModel>() {
                    @Override
                    public void onResponse(Call<BalanceModel> call, Response<BalanceModel> response) {
                        progressDialog.dismiss();
                        if(response.body() != null && response.body().getError() == null){
                            user.balance = response.body().getBalance();
                            user.PriceTarif = response.body().getTarif_pay();
                            user.Tarif = response.body().getTarif();
                            user.TypeConn = user.getLTE() ? getString(R.string.info_type_4G) : getString(R.string.info_type_lan);
                            binding.setVariable(BR.UserAcc, user);
                            binding.executePendingBindings();
                        }else if (response.body() != null && response.body().getError() != null) {
                            ((TextView)findViewById(R.id.userScreenBalance)).setText(response.body().getError());
                        }
                    }
                    @Override
                    public void onFailure(Call<BalanceModel> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
    @Override
    public void onBackPressed(){
        intent = new Intent();
        intent.putExtra("Balance", user.balance);
        intent.putExtra("DateCheck", user.Date_Last_Check);
        setResult(RESULT_OK, intent);
        finish();
    }
}
