package com.example.first;

public class UserModel {
    public int lte;
    public String balance;
    public String ID;
    public String FIO;
    public String Date_Create;
    public String Date_Last_Check;
    String Tarif = "ЭКОНОМ";
    String TypeConn = "Проводной интернет";
    String PriceTarif = "695.00";

    public UserModel(String balance, int lte, String ID, String FIO, String Date_Create, String Last_Check){
        this.balance = balance; this.lte = lte; this.ID = ID; this.FIO = FIO;
        this.Date_Last_Check = Last_Check;
        this.Date_Create = Date_Create;
    }

    public String getID(){
        return ID;
    }

    public String getFIO(){
        return FIO;
    }

    public Boolean getLTE(){
        return lte == 1;
    }

    public String getBalance(){
        return balance;
    }

    public String getTarif(){
        return Tarif;
    }

    public String getTypeConn(){
        return TypeConn;
    }

    public String getPriceTarif(){
        return PriceTarif;
    }
}
