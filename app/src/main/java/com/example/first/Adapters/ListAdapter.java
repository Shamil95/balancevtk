package com.example.first.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.first.R;
import com.example.first.UserModel;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private List<UserModel> usersList;
    private ItemClickListener mClickListener;

    public ListAdapter(List<UserModel> users){
        this.usersList = users;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder userHolder, int i) {
        userHolder.bind(usersList.get(i));
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        //private final ViewDataBinding binding;


        TextView itemImage;
        TextView itemBalance, itemID, itemFIO, itemDateCreate, itemLastCheck;

        public ViewHolder(View itemView){
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_img);
            itemBalance = itemView.findViewById(R.id.item_balance);
            itemID = itemView.findViewById(R.id.item_ID);
            itemFIO = itemView.findViewById(R.id.item_FIO);
            itemDateCreate = itemView.findViewById(R.id.item_date_create);
            itemLastCheck = itemView.findViewById(R.id.item_last_check);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(view, getAdapterPosition());
        }

        public void bind(UserModel user){
            if(user.lte == 1)
            {
                itemImage.setBackgroundResource(R.drawable.fourg);
                itemImage.setText(R.string.TypeFourG);
            }
            else{
                itemImage.setBackgroundResource(R.drawable.lan);
                itemImage.setText(R.string.TypeLAN);
            }
            itemBalance.setText(user.balance + "");
            itemID.setText(user.ID);
            itemFIO.setText(user.FIO);
            itemDateCreate.setText(user.Date_Create);
            itemLastCheck.setText(user.Date_Last_Check);
        }
    }

    public UserModel getItem(int id) {
        return usersList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
