package com.example.first.Adapters;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class FactoryAdapter implements TypeAdapterFactory {

    private String tagResult = "result";

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            @Override
            public T read(JsonReader in) throws IOException {
                JsonElement jsonElementRoot = elementAdapter.read(in);
                if (jsonElementRoot.isJsonObject()) {
                    JsonObject jsonObject = jsonElementRoot.getAsJsonObject();
                    if (jsonObject.has(tagResult) )
                    {
                        JsonElement jsonElement = jsonObject.get(tagResult);

                        if(jsonElement.isJsonArray()) {
                            return delegate.fromJsonTree(jsonElement);
                        } else{
                            return delegate.fromJsonTree(jsonElement);
                        }
                    }
                }
                return delegate.fromJsonTree(jsonElementRoot);
            }
        }.nullSafe();
    }
}

