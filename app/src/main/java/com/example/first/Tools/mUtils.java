package com.example.first.Tools;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.view.View;

import java.util.Calendar;

public class mUtils {
    public static String getFormattedDate(Context ctx, Long smsTimeInMills){
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMills);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "H:mm";
        final String dateTimeFormatString = "EEEE, MMMM d:, H:mm";

        if(now.get(Calendar.DATE) == smsTime.get(Calendar.DATE))
            return "" + DateFormat.format(timeFormatString, smsTime);
        else if (now.get(Calendar.DATE) -smsTime.get(Calendar.DATE) == 1)
            return "Вчера " + DateFormat.format(timeFormatString, smsTime);
        else if(now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR))
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        else
            return DateFormat.format("dd MMMM yyyy, H:mm", smsTime).toString();
    }
}
