package com.example.first.Retrofit;

import android.app.Application;

import com.example.first.Adapters.FactoryAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService extends Application {

    private static NetworkService mInstance;

    public static NetworkService getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkService();
        }
        return mInstance;
    }

    private static final String BASE_URL = "http://debishev.net/vtk.balance/";
    private Retrofit mRetrofit;
    public static RetrofitHelper retrofitHelper;

    private NetworkService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory( new FactoryAdapter())
                /*.enableComplexMapKeySerialization()
                .serializeNulls()
                .setDateFormat(DateFormat.LONG)
                .setPrettyPrinting()
                .setVersion(1.0)*/ //просто строка
                .create();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        retrofitHelper = mRetrofit.create(RetrofitHelper.class);
    }

}

