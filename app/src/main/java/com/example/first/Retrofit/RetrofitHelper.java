package com.example.first.Retrofit;

import com.example.first.User.BalanceModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RetrofitHelper {
    @FormUrlEncoded
    @POST("balance/")
    Call<BalanceModel> getBalance(@Field("account") String account);
}
