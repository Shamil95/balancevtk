package com.example.first.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class DialogDropItem extends DialogFragment {

    public interface IDialogDrop{
        void OnOkDialogDrop(String DialogDropType);
    }

    IDialogDrop dialogDropItem;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstance){
        try {
            dialogDropItem = (IDialogDrop) getActivity();
        }catch (ClassCastException e){
            throw new ClassCastException(getActivity(). toString() + " must implement IDialogDropItem");
        }
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setMessage("Удалить договор?").setTitle("Удаление договора");
        adb.setPositiveButton("Удалить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogDropItem.OnOkDialogDrop("DropItem");
            }
            }).setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            });
        return adb.create();
    }
}
