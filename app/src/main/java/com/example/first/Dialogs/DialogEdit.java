package com.example.first.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.first.R;
import com.example.first.UserModel;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DialogEdit extends DialogFragment implements View.OnClickListener {

    DialogAdd.IDialog iDialogEdit;
    UserModel user;

    public void setUserActivity(UserModel user){
        this.user = user;
    }
    View view;
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstance){
        try {
            iDialogEdit = (DialogAdd.IDialog) getActivity();
        }catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString()
                    + " must implement NoticeDialogListener");
        }
        view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_item, null);
        view.findViewById(R.id.btnDialogOK).setOnClickListener(this);
        view.findViewById(R.id.btnDialogCancel).setOnClickListener(this);
        ((EditText)view.findViewById(R.id.IDDogovorDialog)).setText(user.ID);
        ((EditText)view.findViewById(R.id.NameDogovorDialog)).setText(user.FIO);
        if(user.lte == 1)
            ((RadioButton)view.findViewById(R.id.rad4G)).setChecked(true);
        else
            ((RadioButton)view.findViewById(R.id.radLAN)).setChecked(true);
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

        return adb.setTitle("Сохранение данных").setView(view).create();
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.btnDialogOK:
                String date = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
                user = new UserModel("0", ((RadioButton)view.findViewById(R.id.rad4G)).isChecked() ? 1 : 2,
                        ((TextView)view.findViewById(R.id.IDDogovorDialog)).getText().toString(),
                        ((TextView)view.findViewById(R.id.NameDogovorDialog)).getText().toString(),
                        date, date
                );
                iDialogEdit.onOkDialog("EditDogovor", user);
                dismiss();
                break;
            case R.id.btnDialogCancel:
                dismiss();
                break;
                default:
                    break;

        }
    }
}
