package com.example.first.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import com.example.first.R;
import com.example.first.UserModel;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DialogAdd extends DialogFragment{

    public UserModel user;

    public interface IDialog {
        void onOkDialog(String DialogType, UserModel user);
    }

    IDialog DialogAdd;
    View view;
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstance){
        try {
            DialogAdd = (IDialog) getActivity();
        }catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + " must implement IDialogAddListener");
        }


        view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_item, null);
        view.findViewById(R.id.btnDialogOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                user = new UserModel("0", ((RadioButton)view.findViewById(R.id.rad4G)).isChecked() ? 1 : 2,
                        ((TextView)view.findViewById(R.id.IDDogovorDialog)).getText().toString(),
                        ((TextView)view.findViewById(R.id.NameDogovorDialog)).getText().toString(),
                        sdf.format(new Date()), sdf.format(new Date())
                        );
                DialogAdd.onOkDialog("AddDogovor", user);
                dismiss();
            }
        });
        view.findViewById(R.id.btnDialogCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        return adb.setTitle("Сохранение данных").setView(view).create();
    }
}
