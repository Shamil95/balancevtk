package com.example.first.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class DialogDropAllItems extends DialogFragment {

    DialogDropItem.IDialogDrop dialogDropItem;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstance){
        try {
            dialogDropItem = (DialogDropItem.IDialogDrop)getActivity();
        }catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + " must implement IDialogDropItem");
        }
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setMessage("Удалить все договора?").setTitle("Удаление всех договоров");
        adb.setPositiveButton("Удалить все", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogDropItem.OnOkDialogDrop("DropAllItems");
            }
        }).setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        return adb.create();
    }
}
