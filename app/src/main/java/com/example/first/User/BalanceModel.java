package com.example.first.User;

public class BalanceModel {
    private String balance;
    private String tarif;
    private String tarif_pay;
    private String error;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public String getTarif_pay() {
        return tarif_pay;
    }

    public void setTarif_pay(String tarif_pay) {
        this.tarif_pay = tarif_pay;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
