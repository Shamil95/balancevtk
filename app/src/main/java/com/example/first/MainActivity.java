package com.example.first;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.example.first.Adapters.ListAdapter;
import com.example.first.Dialogs.DialogAdd;
import com.example.first.Dialogs.DialogDropAllItems;
import com.example.first.Dialogs.DialogDropItem;
import com.example.first.Dialogs.DialogEdit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements ListAdapter.ItemClickListener, DialogAdd.IDialog, DialogDropItem.IDialogDrop {
    RecyclerView recyclerView;
    FloatingActionButton btnAdd;

    ArrayList<UserModel> users = new ArrayList<>();
    ListAdapter adapter;
    UserModel user;

    DialogFragment dialogAdd;
    DialogFragment dialogEdit;
    DialogFragment dialogDropItem;
    DialogFragment dialogDropAllItems;

    int positionEdit = 0;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialSettings();
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAdd.show(getSupportFragmentManager(), "DialogAdd");
            }
        });
    }

    @Override
    public void onItemClick(View view, final int position) {
        user = adapter.getItem(position);
        view.findViewById(R.id.menu_item_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionEdit = position;
                ((DialogEdit) dialogEdit).setUserActivity(adapter.getItem(position));
                showPopupMenu(v);
            }
        });
        intent = new Intent(this, UserActivity.class);
        intent.putExtra("lte", user.lte);
        intent.putExtra("ID", user.ID);
        intent.putExtra("FIO", user.FIO);
        intent.putExtra("DateCreate", user.Date_Create);
        intent.putExtra("LastCheckDate", user.Date_Last_Check);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;

        user.balance = data.getStringExtra("Balance");
        user.Date_Last_Check = data.getStringExtra("DateCheck");
        user.balance = user.balance.substring(0, user.balance.indexOf('.')) + "р.";

        db = dbHelper.getWritableDatabase();
        cv = new ContentValues();
        cv.put("balance", user.getBalance());
        cv.put("Date_Last_Check", user.Date_Last_Check);

        db.update("Users", cv, "DogovorID = " + user.ID,null);
        adapter.notifyDataSetChanged();
    }

    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.menu_edit_edit) {
                    dialogEdit.show(getSupportFragmentManager(), "DialogEdit");
                    adapter.notifyDataSetChanged();
                }
                else if(menuItem.getItemId() == R.id.menu_edit_drop)
                {
                    dialogDropItem.show(getSupportFragmentManager(), "DialogDropItem");
                }

                return false;
            }
        });
        popupMenu.inflate(R.menu.menu_edit);
        popupMenu.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == R.id.main_menu_clear)
        {
            dialogDropAllItems.show(getSupportFragmentManager(), "DialogDropAllItems");
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void AddUsers(){
        db = dbHelper.getWritableDatabase();
        Cursor c = db.query("Users", null,null, null, null,null,null);
        if(c.moveToFirst()){
            do {
                user = new UserModel(c.getString(c.getColumnIndex("balance")),
                        c.getInt(c.getColumnIndex("lte")), c.getString(c.getColumnIndex("DogovorID")),
                        c.getString(c.getColumnIndex("NameDogovor")), c.getString(c.getColumnIndex("Date_Create")),
                        c.getString(c.getColumnIndex("Date_Last_Check")));
                users.add(user);
            } while (c.moveToNext());
        }
    }
    DBHelper dbHelper;
    SQLiteDatabase db;
    ContentValues cv;
    public void initialSettings(){
        dbHelper = new DBHelper(getApplicationContext());
        dialogAdd = new DialogAdd();
        dialogEdit = new DialogEdit();
        dialogDropItem = new DialogDropItem();
        dialogDropAllItems = new DialogDropAllItems();
        recyclerView = findViewById(R.id.usersList);
        btnAdd= findViewById(R.id.floatingBtn);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        AddUsers();
        adapter = new ListAdapter(users);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onOkDialog(String DialogNumber, UserModel user) {
        db = dbHelper.getWritableDatabase();
        cv = new ContentValues();
        cv.put("DogovorID", user.ID);
        cv.put("NameDogovor", user.FIO);
        cv.put("balance", user.balance);
        cv.put("lte", user.lte);
        cv.put("Date_Create", user.Date_Create);
        cv.put("Date_Last_Check", user.Date_Last_Check);
        switch (DialogNumber) {
            case "AddDogovor":
                users.add(user);
                db.insert("Users", null, cv);
                adapter.notifyDataSetChanged();
                break;
            case "EditDogovor":
                users.get(positionEdit).lte = user.lte;
                users.get(positionEdit).ID = user.ID;
                users.get(positionEdit).FIO = user.FIO;
                users.get(positionEdit).Date_Last_Check = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(new Date());
                db.update("Users", cv, "DogovorID = " + user.ID,null);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void OnOkDialogDrop(String DialogDropType) {
        switch (DialogDropType) {
            case "DropItem":
                users.clear();
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete("Users", null, null);
                adapter.notifyDataSetChanged();
                break;
            case "DropAllItems":
                users.remove(user);
                db = dbHelper.getWritableDatabase();
                db.delete("Users", "DogovorID = " + user.ID, null);
                adapter.notifyDataSetChanged();
                break;
        }
    }
}

